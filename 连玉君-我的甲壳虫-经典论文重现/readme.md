

&emsp;

---

>>> [课程主页-幻灯片+板书](https://gitee.com/arlionn/paper101)

---
&emsp;

&emsp; 


#### 嘉宾简介

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连玉君工作照100.JPG)

**[连玉君](http://lingnan.sysu.edu.cn/node/151)** ，经济学博士，中山大学岭南学院副教授，博士生导师。已在 **China Economic Review**，**经济研究**，**管理世界**，**金融研究** 等期刊发表论文 60 余篇。连玉君老师团队一直积极分享 Stata 应用经验，创办了公众号「Stata连享会 (StataChina)」，开设了 [[Stata连享会-简书]](https://www.jianshu.com/u/69a30474ef33)，[[Stata连享会-知乎]](https://www.zhihu.com/people/arlionn) 两个专栏，累计阅读量超过 200 万人次。

### 课程概览

- **听课方式：** 网络直播。报名后点击邀请码进入在线课堂收看，无需安装任何软件。支持手机、iPad、电脑等。
- **直播嘉宾**：连玉君 (中山大学)
- **费用/时长**：150 元；分 A、B 两次课，共 6 小时
- [**报名主页**](https://lianxh.duanshu.com/#/brief/course/c3f79a0395a84d2f868d3502c348eafc)
- **课程电子包：** 报名后，点击【课程表】获取 PPT, 电子板书, 基于中国数据重现 Flannery and Rangan (2006) 的所有数据和 Stata 代码。 



### 课程提要

> **重现经典论文：公司资本结构的动态调整行为 | Flannery and Rangan([2006](https://quqi.gblhgk.com/s/880197/sjBLG6VYeZPgmpZT))**
- **目标：** 拆解一篇经典论文，学习研究设计、实证方法和选题切入点。
- **讲解重点：** 
  - **完整重现：** 从数据导入、变量生成、数据清洗到回归分析和结果解释，全程用 Stata 重现论文中的核心结果。
  - **评价和延伸：** 结合前期和后续文献，讨论其局限和可能的选题方向 (涉及 20 余篇文献)。
- **文献：** Flannery, M. J., K. P. Rangan, **2006**, Partial adjustment toward target capital structures, ***Journal of Financial Economics***, 79 (3): 469-506. [PDF](https://quqi.gblhgk.com/s/880197/sjBLG6VYeZPgmpZT)
- **文章简介：** 资本结构的动态调整行为并不是一个新的研究话题，但他一直在持续被讨论。这篇论文以权衡理论为基础，在部分调整模型架构下研究了美国上市公司资本结构的动态调整行为。作者发现，若不考虑公司的个体效应，则调整速度会被严重低估，为此，他们使用了固定效应模型 (**FE**)，估得的调整速度约为前期文献的三倍，达到 0.38。问题在于，这一结果可信吗？如果使用更为合理的 **FD-GMM** 进行估计，结果又将如何呢？后续文献，以及 2019-2020 年的文献是如何扩展开来的？
- **方法：** 
  - 混合 OLS (Pooled OLS)
  - Fama-MacBeth 估计 (FM method)
  - 固定效应模型 (FE)
  - 动态面板模型 (Dynamic Panel data)
  - 各种稳健性检验设计
    - 排除替代性假说
    - 变量的衡量偏误
    - 分组回归及组间系数差异检验
    - 短期效应和长期效应



### 拓展阅读

> [浏览课程包中的所有论文(20余篇)](https://quqi.com/s/880197/Rwx0OXfYekbdleFs/616) 

> **Note：** 这些论文与主讲论文密切关联，作为延伸课外阅读资料，授课中仅用于举例说明如何拓展研究方向，不做深入解读。提供其中两篇的重现资料。

- Flannery, M. J., K. W. Hankins, **2013**, Estimating dynamic panel models in corporate finance, *Journal of Corporate Finance*, 19: 1-19.  [[PDF]](https://quqi.gblhgk.com/s/880197/I7UEItHQ7cViZZTV) 
- Faulkender, M., M. J. Flannery, K. Watson Hankins, J. M. Smith, **2012**, Cash flows and leverage adjustments, *Journal of Financial Economics*, 103 (3): 632-646. [[PDF]](https://quqi.gblhgk.com/s/880197/cHUovupfEm26ZJgU)
- Öztekin, Ö., M. J. Flannery, **2012**, Institutional determinants of capital structure adjustment speeds, *Journal of Financial Economics*, 103 (2012): 88-112. [[PDF]](https://quqi.gblhgk.com/s/880197/8fPARMHgfDisniK8)
- Lemmon, M. L., M. R. Roberts, J. F. Zender, **2008**, Back to the beginning: Persistence and the cross-section of corporate capital structure, *Journal of Finance*, 63 (4): 1575-1608. [[PDF]](https://quqi.gblhgk.com/s/880197/Vn9aCY6LRihENW6M)
  - Malmendier, U., G. Tate, J. Yan, **2011**, Overconfidence and early-life experiences: The effect of managerial traits on corporate financial policies, *Journal of Finance*, 66 (5): 1687-1733. [[PDF]](https://quqi.gblhgk.com/s/880197/YAcOc1T5DJo6oiuY)
- 姜付秀, 屈耀辉, 陆正飞, 李焰, **2008**, 产品市场竞争与资本结构动态调整, **经济研究**, (04): 99-110.  [[PDF]](http://www.erj.cn/UploadFiles/%E4%BA%A7%E5%93%81%E5%B8%82%E5%9C%BA%E7%AB%9E%E4%BA%89%E4%B8%8E%E8%B5%84%E6%9C%AC%E7%BB%93%E6%9E%84%E5%8A%A8%E6%80%81%E8%B0%83%E6%95%B4.pdf)
- 姜付秀, 黄继承, **2011**, 市场化进程与资本结构动态调整, **管理世界**, (3): 124-134. [[PDF]](https://quqi.gblhgk.com/s/880197/3EzFHn62hUutiYpm)
- 张胜, 张珂源, 张敏, **2017**, 银行关联与企业资本结构动态调整, **会计研究**, (2): 49-55. [[PDF]](https://quqi.gblhgk.com/s/880197/Yiw9tvGb5xX0gQ3M)
- 黄继承, 阚铄, 朱冰, 郑志刚, **2016**, 经理薪酬激励与资本结构动态调整, **管理世界**, (11): 156-171. [[PDF]](https://quqi.gblhgk.com/s/880197/9zHsDtsIKPi5S8GW)
- 程六兵, 叶凡, 刘峰, **2017**, 资本市场管制与企业资本结构, **中国工业经济**, (11): 155-173. [[PDF]](https://quqi.gblhgk.com/s/880197/LBJkG0dYepJ7OeEy), [Link - Data & Prog](http://www.ciejournal.org/Magazine/show/?id=52161) 
- 王朝阳, 张雪兰, 包慧娜, **2018**, 经济政策不确定性与企业资本结构动态调整及稳杠杆, **中国工业经济**, (12): 134-151. [[PDF]](https://quqi.gblhgk.com/s/880197/tluApR89annpv6hX), [Link-Stata dofiles](http://www.ciejournal.org/Magazine/show/?id=55083)


### 先导课程

这篇论文中综合应用了多种计量方法，包括：混合 OLS、固定效应、动态面板 (POLS, FE, FD-GMM, SYS-GMM)、蒙特卡洛模拟、分组回归等。建议各位在开课前先了解一下这些方法和模型。相关课程如下： 

- **D.** 动态面板：[直播-动态面板数据模型](https://efves.duanshu.com/#/brief/course/3c3ac06108594577a6e3112323d93f3e), 连玉君. 时长: 2小时40分. [「课程详情」](https://www.lianxh.cn/news/594aa12c096ca.html)
- **C.** 研究设计：[直播-实证研究设计](https://mp.weixin.qq.com/s/NGwsr92_Vr1DGRbVqDVQIA)，连玉君. 时长: 2小时30分. [「课程详情」](https://www.lianxh.cn/news/2f31aa3347e83.html)    
- **B.** 静态面板： [直击面板数据模型](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38)，连玉君，**免费**. 时长: 1小时40分, [「课程详情」](https://gitee.com/arlionn/PanelData)     
- **A.** 计量基础： [优酷 - Stata 公开课](https://i.youku.com/arlion)，连玉君，**免费**. 优酷视频，34 节课 (15分钟/节)。
 

&emsp;

## 报名和课程群

#### [- **点我报名** -](https://lianxh.duanshu.com/#/brief/course/c3f79a0395a84d2f868d3502c348eafc)       
> 网址: <https://lianxh.duanshu.com/#/brief/course/c3f79a0395a84d2f868d3502c348eafc>     

或扫码报名：     

<img style="width: 150px" src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/P01-我的甲壳虫-报名二维码.png">

### 课程群
- **课程咨询：** 李老师-18636102467（微信同号）
- **扫码加入课程群，以便课前提问和答疑**

<img style="width: 150px" src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/P01-我的甲壳虫-群二维码.png">


--- 

## 更多精彩课程

> [连享会](https://www.lianxh.cn)-直播平台上线了！      
> &emsp;         
> <http://lianxh.duanshu.com>      
> &emsp;         
<img style="width: 170px" src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会短书直播间-二维码170.png">

> 长按二维码观看视频课程 (此前已经直播完的课程仍然可以随时购买观看)。



&emsp;

---
>#### 关于我们

- **Stata连享会** 由中山大学连玉君老师团队创办，定期分享实证分析经验。
- &#x1F33A; **你的颈椎还好吗？** 为了保护颈椎，您可以前往 [::连享会-主页::](https://www.lianxh.cn) (在浏览器地址栏中输入 lianxh.cn 即可) 或 [::连享会-知乎专栏::](https://www.zhihu.com/people/arlionn/) (在知乎中搜索「连享会」即可) 查看往期推文，并将上述网址添加到浏览器收藏夹中或把您喜欢的推文【收藏】起来。
- &#x1F4C1; **往期推文分类查看：** [计量专题](https://mp.weixin.qq.com/mp/homepage?__biz=MzAwMzk4ODUzOQ==&hid=4&sn=0c34b12da7762c5cabc5527fa5a1ff7b) | [分类推文](https://mp.weixin.qq.com/mp/homepage?__biz=MzAwMzk4ODUzOQ==&hid=2&sn=07017b31da626e2beab0332f5aa5f9e2) | [资源工具](https://mp.weixin.qq.com/mp/homepage?__biz=MzAwMzk4ODUzOQ==&hid=3&sn=10c2cf37e172289644f03a4c3b5bd506)。推文分成  **内生性** | **空间计量** | **时序面板** | **结果输出** | **交乘调节** 五类，主流方法介绍一目了然：DID, RDD, IV, GMM, FE, Probit 等。
- &#x1F4A5; **公众号关键词搜索/回复** 功能已经上线。大家可以在公众号左下角点击键盘图标，输入简要关键词，以便快速呈现历史推文，获取工具软件和数据下载。常见关键词：`DID，RDD，PSM，面板，IV，合成控制法，内生性, Probit, plus，Profile, Bootstrap, MC, 交乘项, 平方项, 工具, 软件, Sai2, gInk, Annotator, 手写批注, 直击面板数据, 连老师, 直播, 空间计量, 爬虫, 文本分析, 正则, Markdown幻灯片, marp, 盈余管理, ` ……。

---

<img style="width: 300px" src="https://images.gitee.com/uploads/images/2020/0318/090822_8d473867_1522177.png">








