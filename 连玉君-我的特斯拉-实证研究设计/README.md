## 连享会Live - 我的特斯拉-实证研究设计

- **听课方式：** 可以通过手机、iPad、电脑在线观看、学习。附带 6 篇 Top 期刊的 Stata 实现过程，涉及 PSM+DID，RDD 等方法。
- **直播嘉宾**：连玉君 (中山大学)
- **费用/时长**：88 元 (2 小时 20 分)。
- **报名**：扫描二维码，或直接点击 [[听课链接]](https://efves.duanshu.com/#/brief/course/5ae82756cc1b478c872a63cbca4f0a5e?classId=423589)。
- **相关课程：** [2020连享会-文本分析与爬虫-4天直播](https://mp.weixin.qq.com/s/s_gaglmdKB46yFfy3-fwrg)  |  [PDF](https://quqi.com/s/880197/YvkIskOPpc2lnqxM) 


> #### [直播课程：实证研究设计 (2.4小时)](https://mp.weixin.qq.com/s/NGwsr92_Vr1DGRbVqDVQIA)

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/海报-研究设计回放-短书-特斯拉.png)



&emsp;


---

## $\color{red}{New!}$ 课程资料


### 课程 PPT 和涉及的论文 (Free)
- 课程 PPT，[「在线浏览 - 带板书」](https://quqi.gblhgk.com/s/880197/P0VCY1c2Pzp68R2w)；[在线浏览 - 原版](https://quqi.gblhgk.com/s/880197/FMoueKCUCLZjedLa)
- 包含 20 篇发表于 JFE, JF, 经济研究等期刊的论文，[「在线浏览」](https://quqi.com/s/880197/kfhYJIve329ras5J)

### 完整课程资料 (付费后获取)
购买课程后联系客服，获取完整课程电子包，包含如下资料：
1. 【Sample_Flannery_2006】文件夹里存放了一篇 JFE 论文的完整实现过程
- 使用方法：请将【Sample_Flannery_2006】解压后放置于 E 盘根目录下，在 Stata 命令窗口中执行如下命令即可打开 dofile，重现该文实证分析过程：
  ```
  . cd E:\连玉君-研究设计\Sample_Flannery_2006
  . doedit Flannery_2006.do
  ```
2. 【refs】提供了 PPT 中涉及的 20 篇论文的 PDF 原文；
3. 【data】中放置了 5 篇论文的 Stata 实现过程，涉及 RDD，PSM+DID,IV 估计等方法。

### 常见问题解答
- **途径1**：提问之前，请浏览 [「连享会-简书」](https://www.jianshu.com/u/69a30474ef33) 和 [「连享会-知乎」](https://www.zhihu.com/people/arlionn/) 搜索关键词，多数问题都已经在我们之前发布的 200 多篇推文中解释过。
- **途径2**：可以前往 [「连享会-现场班常见问题解答 FAQs」](https://gitee.com/arlionn/Course/wikis/Stata%E5%88%9D%E7%BA%A7%E7%8F%AD_FAQs.md?sort_id=1551499) 页面查看。
- **途径3**：若仍有疑问，可以本主页下方留言，我们会尽快回应。若问题比较复杂，可以发邮件只 <StataChina@163.com>，提问前请认真阅读 [「如何提问？」](https://www.jianshu.com/p/9870080fe769)，否则你可能无法收到回复邮件。



&emsp;

---

### 课程内容

- 一篇实证分析论文的流程
- 模型选取和设定
- 内生性的来源和主要处理方法
- 稳健性检验如何做？
- 如何重现经典论文 ? (选题和方法来源)
- 从哪里获取重现论文的数据和程序？
- 分享一份 dofile 模板 (顺风车)

### 课程特色

- **深入浅出**：掌握最主流的实证分析思路和方法
- **浸入式教学**：全程电子板书+Stata 操作演示，课后分享电子板书
- **数据和程序**：分享全套 Stata 课件 (PPT、数据、程序和 dofiles)，以便重现课程中演示的所有结果


&emsp;

--- 

## 嘉宾简介

![连玉君](https://images.gitee.com/uploads/images/2019/0801/231203_00d532e3_1522177.jpeg)   

**[连玉君](http://lingnan.sysu.edu.cn/node/151)** ，经济学博士，副教授，博士生导师。2007年7月毕业于西安交通大学金禾经济研究中心，现任教于中山大学岭南学院金融系。主讲课程为“金融计量”、“实证金融”等。已在《China Economic Review》、《经济研究》、《管理世界》、《经济学(季刊)》、《金融研究》、《统计研究》等期刊发表论文 60 余篇；主持完成国家自然科学基金项目、教育部人文社科基金项目、广东自然科学基金项目等课题项目 10 余项。目前已完成 Panel VAR、Panel Threshold、Two-tier Stochastic Frontier 等计量模型的 Stata 实现程序，并编写过几十个小程序，如 `xtbalance`, `winsor2`, `bdiff`, `hausmanxt`, `ttable3`, `hhi5`, `ua`等。连玉君老师团队一直积极分享 Stata 应用经验，创办了公众号「Stata连享会 (StataChina)」，开设了 [[Stata连享会-简书]](https://www.jianshu.com/u/69a30474ef33)，[[Stata连享会-知乎]](https://www.zhihu.com/people/arlionn) 两个专栏，累计阅读量超过 200 万人次。



&emsp;


---
### 相关课程

&emsp;

> #### [2020连享会 - 文本分析与爬虫 - 4天直播](https://mp.weixin.qq.com/s/s_gaglmdKB46yFfy3-fwrg)  |  [PDF](https://quqi.com/s/880197/YvkIskOPpc2lnqxM)  
> 主讲嘉宾：司继春 || 游万海

![连享会-文本分析与爬虫专题-4天直播](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lan-文本分析-海报.png "连享会-文本分析与爬虫-4天直播，2020.3.28-29，4.4-5")

