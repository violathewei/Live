

&emsp;


## 连享会直播：效率分析专题 (TFP-DEA-SFA)


![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会-效率专题海报-400.png)




> 连享会小程序：扫一扫，看推文，看视频……

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会小程序二维码180.png)


## 1. 课程概览

- **简介：** 本课程通过三个专题的讲解，系统介绍各类效率分析工具的原理和 Stata 实现方法，包括： **TFP  专题** (全要素生产率)、**DEA 专题** (数据包络分析)、**SFA 专题** (随机边界分析)。每个主题提供 1-2 篇论文的讲解，附带完整的重现文件，以便大家能应用于自己的实证分析之中。[课程大纲 PDF 版本](https://quqi.gblhgk.com/s/880197/g3ne5HdXdrSoo8iL)
- **课程主页：** <https://gitee.com/arlionn/TE> 
- **授课方式：网络直播** (15 天回放)，缴费后主办方发送邀请码点击邀请码即可进入在线课堂，收看直播无需安装任何软件。
- 时间：2020 年 5 月 29-31 日 (周五-周日)
  - **时段：** 上午 9:00-12:00; 下午 2:30-5:30；课后 30-60 分钟答疑
- **授课嘉宾：**
  - 鲁晓东 (中山大学)
  - 张 &ensp; 宁 (暨南大学)
  - 连玉君 (中山大学)
- **软件/课件：** Stata 软件，提供全套 Stata 实操程序、数据和 dofiles (开课前一周发送)
- **报名链接：** <http://junquan18903405450.mikecrm.com/FhWLhS2>


&emsp;

## 2. 课程缘起

2010 年春，我和晓东在中大岭院「荣光堂」吃午餐，聊起了工业企业数据库的处理问题。当时一位同事花费「巨资」购买了 1998-2007 年的工业企业数据库原始数据，但一直无奈地让它躺在那里睡觉，因为没人知道如何整理这套庞大又有些杂乱的数据。

我内心「呵呵」了一下，心想「有那么困难吗？」。随后两周，我完全投入到这份数据的清理工作中，突破了几个核心障碍后，其它的基本上就是体力活了。一是把 Access 格式的数据转换成 Stata 格式，最终选择了 **Stat Transfer** 软件；二是各年度变量名不一致问题，这没有什么好办法，最终使用 Excel 逐年手工对照出来，形成了一份花花绿绿的 Excel 表格，并把变量命名为 a1, a2, …… 的形式；三是数据合并问题，由于内存不够，只好分年度处理、精简后再拼接。当然，还有其他各类小问题，都用香烟一一化解掉了。

有米下锅，后面的事情就简单了。我们要做一个基础性工作——对比各类 TFP 估算方法。这项工作只用了两个月就完成了，形成了 [鲁晓东和连玉君](https://quqi.gblhgk.com/s/880197/zXaRhRydMP6Q1ejw) (2012, 经济学季刊) 这篇论文。时至今日，我们大约收到了 100 封交流和求助邮件。按照当时在论文中的承诺，我们都一一进行了回复，提供了 dofile。

这篇文章的引用率也超乎我们的想象。根据 [「知网-引文报告」](http://kns.cnki.net/kcms/detail/detail.aspx?dbcode=CJFD&dbname=CJFD2012&filename=JJXU201202010&v=MjY4MDdmWStSc0Z5dmtVcnpCTHlmVGU3RzRIOVBNclk5RVpJUUtESDg0dlI0VDZqNTRPM3pxcUJ0R0ZyQ1VSN3E=)，该文被引 826 次 (截至 2020.4.23)，二级引证高达 5010 次。大家对此问题的关注并未随着时间的推移而下降，仅看最近两年 (2018-2020)，引证文献中包括《经济研究》(4 篇), 《管理世界》(5 篇), 《中国社会科学》(1 篇), 《中国工业经济》 (6 篇, 登陆官网可以查看数据和程序), ……。研究的话题包括：创新、生产率、配置效率、僵尸企业、营改增、进出口、高铁网络等等，涉及宏观经济、区域经济、企业行为等多个维度，[「查看详情」](https://gitee.com/arlionn/TE/wikis/Lu_Lian_Cite.md?sort_id=2141903)。


我们坚信，有关效率分析的话题永远不会过时。于是，我和晓东筹划着一起开个专题来分享我们过去 10 年中在效率分析研究中积累的方法和经验，也顺便逼迫自己将最新的一些方法加进来。

我们想做一个兼具「深度」和「广度」的专题。由晓东来讲 **TFP 专题**，我来讲 **SFA 专题**，但还缺少另一个效率分析利器——**DEA** 的行家。咨询了几位圈内的好友，他们几乎异口同声地推荐了帅气的「**优青**」学者——暨南大学的张宁教授。他多年来一直专注于资源环境经济，效率与生产率分析方面的问题，提出了多种改进的全要素生产率模型和能源环境效率模型。成果更是耀眼：以第一或通讯在 **Science**、**Nature** 子刊、**Cell** 子刊、**经济研究** 等权威期刊发表论文 70 余篇，18 篇论文进入 ESI 热点和高被引论文。他同时还担任 SSCI 期刊 **Social Science Journal** 副主编，以及 ABS 三星级期刊 **Technological Forecasting and Social Change** 的编委。

这个专题课程筹划了 1 个多月，开了 4-5 次「腾讯会议」。讨论的焦点是：如何在有限的三天时间内，把原理讲透，同时又能涵盖一些相对前沿的方法和模型。最重要的是，我们要在授课中重现哪些论文，以便大家能够在我们提供的课件和 dofile 基础上尽快切换到自己的研究话题上。

想达到让学生「**听懂+会用**」的效果并非易事。这需要老师能深入浅出地讲清楚模型的基本思想、适用条件，结果的解释和呈现，在文献中的应用情况，以及在实操中可能遇到的各种问题。此次授课的三位老师在过去的十年中都一直在使用各自主题中涵盖的模型和方法，有些程序甚至是我们自己编写的。因此，我们有信心让学生们「**听懂**」。那么，如何确保学生们「**会用**」呢？我们最终商议的方案是——案例教学-干中学。在每个专题中，我们都会精讲 1-2 篇期刊论文的 Stata 实现过程，伴以研究过程中各种思考和解决思路。课后，大家可以通过「精读论文+Stata 实操」的方式，逐步吸收、提升。


在内容安排上，第一讲涵盖了有关 TFP (全要素生产率) 估算中的各种方法和陷进，并结合 **中国工业企业数据库** 和鲁晓东老师发表于《经济学季刊》和《世界经济》的两篇论文的 Stata 重现过程来展示这些方法实际应用。

第二讲由张宁老师系统介绍 DEA 在上述领域的效率和生产率分析中的应用，并提供相应的 Stata 程序和实现过程。

第三讲由连玉君老师介绍各类随机边界模型 (SFA)，包括 SFA 估算效率的基本原理、异质性 SFA，双边 SFA，以及新进发展出来的内生性 SFA。

> **课程特色**

- **深入浅出**：掌握最主流的效率分析模型和方法：TFP，DEA，SFA
- **电子板书**：全程电子板书演示，课后分享；听课更专注，复习更高效。
- **讲义程序**：分享全套课件 (数据、程序和论文)，课程中的方法和代码可以应用于自己的论文中。

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/夜幕一舟.png)


&emsp; 


## 3. 第一天：全要素生产率 (TFP) 专题

### 嘉宾简介

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/鲁晓东工作照100.jpg)

鲁晓东，中山大学岭南学院经济学系副主任、教授、博士生导师，中山大学转型与开放经济研究所研究员。2008 年毕业于南开大学，获经济学博士学位。主要研究领域为国际贸易、国际金融与国际投资。迄今已在《经济研究》、《管理世界》、《经济学季刊》、《世界经济》、《金融研究》、《Frontier of Economics in China》等中英文期刊发表学术论文 40 余篇，出版专著 3 部。先后主持国家自然科学基金、国家社会科学基金、教育部人文社科基金项目等课题 12 项。

### 专题导引

全要素生产率 (TFP) 反映了生产过程中各种投入要素的单位平均产出水平，即投入转化为最终产出的总体效率，在经济效率评估中具有不可替代的重要地位。经典的对全要素生产率的测算是从估计生产函数开始的，根据不同的划分标准，TFP 的估计可以分为前沿分析或非前沿分析、确定性方法或者计量方法、宏观方法或微观方法等。

对于微观企业层面的 TFP 估计而言，由于存在「同时性偏差」以及「样本选择偏差」等问题，使得 OLS 估计有偏，从而催生了 **FE 方法、OP 方法、LP 方法、Wooldridge 方法、ACF 方法** 等多种解决方案。以上方法存在什么区别？在实际应用中应该选择那种估计方案？往往令 TFP 实证研究者颇感困扰。

此外，随着中国工业企业数据库的使用日渐广泛，对于中国工业企业 TFP 的估计问题已经成为众多实证研究的基础性工作。但是由于该数据库的非公开性和复杂性，使得不同 TFP 研究者的估计结果无法直接比对，甚至出现相互矛盾的情况，严重降低了 TFP 估计结果的科学性和可信度。

针对以上问题，本讲聚焦于企业层全要素生产率的估计问题，全面梳理 TFP 估计的理论基础和原理，并以中国工业企业随机抽样数据集为例，结合鲁晓东和连玉君 (2012) 的估计，实景演示 Stata 中各种与 TFP 相关命令 (如 `opreg`, `levpet`, `xtabond2`, `acfest`, `prodest`) 的操作过程、参数设置，并就估计结果的解读以及注意的问题予以说明。最后，以鲁晓东 (2014) 为例，展示 TFP 指标更为广泛的应用场景和各类陷阱。


### 授课内容

> 中国工业企业全要素生产率估计：从 OLS 到 ACF

**全要素生产率非估不可吗？**

- 什么是全要素生产率？
- 全要素生产率估计方法分类
- 全要素生产率指标的主要应用领域

**TFP 估算方法的理论依据：全要素生产率很「残」吗？**

- 生产函数的基础设定
- 线性估计方法存在的问题
- 同时性偏差和样本选择偏差
- 其他问题

**估算原理：这一壶旧酒还需要多少新瓶子？**

- 最小二乘法、固定效应方法
- OP 法、LP 法
- Wooldridge 方法
- MrEst 方法、ACF 方法

**不得不说的中国工业企业数据库：你需要绕开的七大陷阱**

- 企业名称跨年匹配
- 名义变量的平减
- 资本存量估计
- 企业进入退出变量生成
- 产业分类编码变更
- 工业增加值测算
- 2004 年普查数据处理

**用 prodest 搞定一切：基于最新工企库的一站式演示**

**论文重现：**
- 鲁晓东, 连玉君. 中国工业企业全要素生产率估计:1999-2007[J]. 经济学:季刊, 2012 (02):179-196. [[PDF]](https://quqi.gblhgk.com/s/880197/zXaRhRydMP6Q1ejw)
- 鲁晓东, 技术升级与中国出口竞争力变迁：从微观到宏观的弥合, **世界经济**, 2014 年第 8 期. [[PDF]](https://quqi.gblhgk.com/s/880197/CMx0UvXy0TTQJr9X)

**参考文献 [PDF原文](https://quqi.gblhgk.com/s/880197/BlEjhDGozwNvE8ZK)**

-  鲁晓东, 连玉君. 中国工业企业全要素生产率估计:1999-2007[J]. **经济学:季刊**, 2012 (02):179-196. [[PDF]](https://quqi.gblhgk.com/s/880197/zXaRhRydMP6Q1ejw)
- 鲁晓东：技术升级与中国出口竞争力变迁：从微观到宏观的弥合，**世界经济**，2014 年第 8 期. [[PDF]](https://quqi.gblhgk.com/s/880197/CMx0UvXy0TTQJr9X)
- Ackerberg, D., K. Caves, and G. Frazer. 2015. Identification Properties of Recent Production Function Estimators. **Econometrica** 83(6): 2411-2451. [[PDF]](https://quqi.gblhgk.com/s/880197/i21uZVGFGFFiJFGI)
- Levinsohn, James and Amil Petrin. 2003. "Estimating Production Functions Using Inputs to Control for Unobservables." **The Review of Economic Studies** 70 (2):pp. 317–341. [[PDF]](https://quqi.gblhgk.com/s/880197/bzdJLtsZVMEhig0k)
- Olley, G.S. and A. Pakes. 1996. "The dynamics of productivity in the telecommunications equipment industry." **Econometrica** 64 (6):1263–1297. [[PDF]](https://quqi.gblhgk.com/s/880197/pwmkRktnOdMels0Z)
- Rovigatti G , Mollisi V .2018,Theory and practice of total-factor productivity estimation: The control function approach using Stata. **Stata Journal,** 18(3):618-662. [[PDF]](https://quqi.gblhgk.com/s/880197/CSMnA86jDQlBEwIr)
- Wooldridge, Jeffrey M. 2009. "On estimating firm-level production functions using proxy variables to control for unobservables." **Economics Letters** 104 (3):112-114. [[PDF]](https://quqi.gblhgk.com/s/880197/yRf7xzelFuyGDhG9)

&emsp;



## 4. 第二天：数据包络分析 (DEA) 专题

### 嘉宾简介

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/张宁工作照新100.jpg)

**张宁**，经济学博士，现任暨南大学经济学院教授、博士生导师，国家优秀青年基金获得者，珠江青年学者，国家重点研发课题负责人。主要研究方向为：资源环境经济，效率与生产率分析等，提出多种改进的全要素生产率模型和能源环境效率模型。现担任 SSCI 期刊《Social Science Journal》副主编；ABS 三星级期刊《Technological Forecasting and Social Change》期刊编委，以第一或通讯在 Science、Nature 子刊、Cell 子刊、《经济研究》等国内外期刊发表论文 70 余篇，18 篇论文进入 ESI 热点和高被引论文。主持 4 项国家基金项目、包括优秀青年基金，重大研究计划，国家重点研发课题，面上项目等，获得霍英东青年教师奖、国家优秀自费留学生等奖励。撰写的多篇研究报告获得各级领导批示，部分政策建议转换为政府决策。

### 专题导引

近年来，数据包络分析 (DEA) 方法，由于其不需要假定估计函数形式等优点，被广泛应用于环境与能源经济学、全要素生产率测算、银行效率和生产率评估等重要领域。

本讲系统介绍 DEA 在上述领域的效率和生产率分析中的应用，并提供相应的 Stata 程序和实现过程。

本讲首先介绍用于技术效率测算的基本径向效率模型 CCR 和 BCC，进而介绍非径向效率模型 SBM。DEA 模型由于缺少统计检验而受到一些批评，因此，我们还会介绍 Bootstrap 方法在 DEA 中的应用，以便估计效率值的置信区间。

全要素生产率被认为长期经济增长的核心动力，估计全要素生产率是经济学研究中重要问题。Färe et al. (1994) 首次将 DEA 模型运用到全要素生产率 (TFP) 的测算中，利用 Malmquist 指数测算 TFP，并将其分解为三个驱动因素。Ray and Desli (1997) 指出 Färe et al. (1994) 的分解错误，提出新的分解思路。 Färe et al. (1994) 提出的基于 DEA 的 TFP 测算，由于估计跨期距离函数会造成不可行解问题，Pastor and Lovell (2005) 提出了 Global Malmquist 指数来解决这一问题。考虑技术非退步假设，Shestalova, (2003) 提出了 Sequential Malmquist 指数，还有考虑分组异质性的 Metafroniter Malmquist 指数 (Oh and Lee, 2010)。本讲将详细介绍这些 TFP 指数的原理和 Stata 实现。

本讲还会介绍考虑非期望产出的方向距离函数模型 (DDF) 和非径向方向距离函数 (NDDF) 在环境效率和能源效率测算中的应用。在生产率方面，还将介绍基于 DDF 构建的 Malmquist-Luenberger (ML) 指数，以及 ML 指数的拓展模型: Global Malmquist-Luenberger (GML), Sequential Malmquist-Luenberger (SML) 和 Metafrontier Malmquist-Luenberger (MML) 指数的原理和 Stata 估计程序。

最后，学以致用，我们会结合论文案例，讲解这些模型在环境能源经济和银行生产率估算中的应用。

### 授课内容

- 传统 DEA 模型 (CCR BCC)
- Bootstrapping DEA 模型
- Slack-based measure (SBM) 模型
- Malmquist index
  - Malmquist 分解之争：FGNZ vs RD (Färe et al., 1994; Ray and Desli, 1997)
  - Global Malmquist Index (Pastor and Lovell, 2005; Shestalova, 2003)
  - Sequential Malmquist Index (Shestalova, 2003)
- Directional distance function –方向距离函数
  - Radial Measures (Chung et al., 1997) 径向方向距离函数
  - Non-radial DDF (Zhou et al., 2012, Zhang et al., 2014) 非径向方向距离函数
  - SBM 方向距离函数在环境和能源效率评估中的应用 (陈诗一, 2012) 
- Malmquist-Luenberger Index 
  - Basic model (Chung et al., 1997)
  - Global Malmquist-Luenberger Index (Oh, 2010)
  - Sequential Malmquist-Luenberger Index (Oh and Heshmati, 2010)
  - Metafrontier Malmquist-Luenberger Index (Oh, 2010)
- Stata 范例 
  - 基本模型在中国工业行业环境全要素生产率应用  (陈诗一，2010) 
  - 在火电企业碳排放全要素生产率中的应用  (Zhang and Choi, 2013) 
  - 在银行全要素生产率中的应用 (Fujii et al., 2014) 

**参考文献： [PDF原文](https://quqi.gblhgk.com/s/880197/e9n0lip7v3V9CQ08)**

- Charnes, A., Cooper, W.W., Rhodes, E., 1978. Measuring the efficiency of decision making units. **European Journal of Operational Research**, 2, 429-444.
- Banker, R.D., Charnes, A., Cooper, W.W., 1984. Some Models for Estimating Technical and Scale Inefficiencies in Data Envelopment Analysis. **Management Science**, 30, pp. 1078-1092.
- Färe, R., Grosskopf, S., Norris, M., & Zhang, Z. (1994) . Productivity Growth, Technical Progress, and Efficiency Change in Industrialized Countries. **American Economic Review**, 84(1), 66-83.
- Ray, S., & Desli, E. (1997) . Productivity Growth, Technical Progress, and Efficiency Change in Industrialized Countries: Comment. **American Economic Review**, 87(5), 1033-1039.
- Tone, K., 2001. A slacks-based measure of efficiency in data envelopment analysis. **European Journal of Operational Research**, 130, 498-509
- Pastor, J., Lovell, C. (2005) . A global Malmquist productivity index, **Economics Letters**, 88(2):266-271.
- Shestalova.V (2003) . Sequential malmquist indices of productivity growth: an application to oecd industrial activities. **Journal of Productivity Analysis**, 19(2-3), 211-226.
- Chung Y, Färe R, Grosskopf S. 1997.Productivity and Undesirable Outputs: A Directional Distance Function Approach. **J Environ Manage;** 51:229-40.
- Zhou P, Ang BW, Wang H. Energy and CO2 emission performance in electricity generation: A non-radial directional distance function approach. **European Journal of Operational Research** 2012;221:625-35.
- Zhang N, Choi Y. Total-factor carbon emission performance of fossil fuel power plants in China: A metafrontier non-radial Malmquist index analysis. **Energy Economics** 2013; 40: 549-59.
- Zhang N., Kong F., Choi Y., Zhou, P. 2014. The effect of size-control policy on unified energy and carbon efficiency for Chinese fossil fuel power plants. **Energy Policy**, 70, 193-200.
- 陈诗一. (2010) . 中国的绿色工业革命:基于环境全要素生产率视角的解释(1980—2008) . **经济研究** (11), 21-34.
- 陈诗一. (2012) . 中国各地区低碳经济转型进程评估. **经济研究** (08), 33-45.
- Oh D. A global Malmquist-Luenberger productivity index. **Journal of Productivity Analysis** 2010;34:183–97
- Oh D. A metafrontier approach for measuring an environmentally sensitive productivity growth index. **Energy Economics** 2010;32:146-57.
- Oh D, Heshmati A. A sequential Malmquist–Luenberger productivity index: Environmentally sensitive productivity growth considering the progressive nature of technology. **Energy Economics** 2010;32:1345-55.
- Fujii, H., Managi, S., Matousek, R. 2014, “Indian bank efficiency and productivity changes with undesirable outputs: A disaggregated approach. **Journal of Banking and Finance**”, Vol.38, 41-50


&emsp;



## 5. 第三天：随机边界分析 (SFA) 专题

### 嘉宾简介

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连玉君工作照100.JPG)

**[连玉君](http://lingnan.sysu.edu.cn/node/151)** ，经济学博士，副教授，博士生导师。2007 年毕业于西安交通大学金禾经济研究中心，现任教于中山大学岭南学院金融系。主讲课程为"金融计量"、"实证金融"等。已在《China Economic Review》、《经济研究》、《管理世界》、《经济学(季刊) 》、《金融研究》、《统计研究》等期刊发表论文 60 余篇；主持完成国家自然科学基金项目、教育部人文社科基金项目、广东自然科学基金项目等课题项目 10 余项。目前已完成 Panel VAR、Panel Threshold、Two-tier Stochastic Frontier 等计量模型的 Stata 实现程序，并编写过几十个小程序，如 `xtbalance`, `winsor2`, `bdiff`, `hausmanxt`, `ttable3`, `hhi5`, `ua`等。连玉君老师团队一直积极分享 Stata 应用经验，创办了公众号「Stata 连享会 (StataChina) 」，开设了 [「连享会-CSDN」](https://blog.csdn.net/arlionn)，[「连享会-知乎」](https://www.zhihu.com/people/arlionn) 两个专栏，累计阅读量超过 200 万人次。

### 专题导引

本讲系统介绍目前日益得到广泛应用的随机边界模型 (SFA)，包括异质性 SFA、面板 SFA 和双边 SFA，它们是产出效率、成本效率、议价能力、投资效率估算等领域的主要分析工具。

异质性随机边界模型有助于我们在估算出无效率程度的同时，可以进一步分析影响非效率程度的因素。

在前期文献中，固定效应面板 SFA 模型的估计是一直是个难题，本课程中提供了  [Greene (2005)](https://quqi.gblhgk.com/s/880197/Ehqseke3eZKpeRvt) 提出的 TFE-SFA (True fixed effect SFA) 模型，以及[Wang and Ho (2010)](https://quqi.gblhgk.com/s/880197/hfCO3ndRfgFiqbCF) 提出的 Scaling-TFE (Scaling True Fixed effect SFA) 模型的 Stata 估计程序，能够很方便地估计包含固定效应的面板 SFA 模型。

双边随机边界模型 (Two-tier SFA) 由 [Kumbhakar, Tsionas and Sipilinen (2009)](https://quqi.gblhgk.com/s/880197/IBH1K4IojWJqxXXf) 提出，在衡量信息不对称程度、投资效率、议价能力等方面有重要应用，如[卢洪友, 连玉君, 卢盛峰 (2011)](https://quqi.gblhgk.com/s/880197/htv0MtfS6JlJtReK) 使用该模型测度了中国医疗市场的信息不对称程度， [任曙明和吕镯 (2014)](https://quqi.gblhgk.com/s/880197/fxwxXlrSLxAfmOsE) 使用该模型测度了有效生产率，进而估算了政府补贴的正效应和和融资约束的负效应。

完成 SFA 估计后，如何估计无效率程度在文献中也存在争议，本课程中提供了一个便捷的命令，可以得到多种文献中常用的无效率估计量，以便作对比分析或稳健性分析。

### 授课内容

- 传统的 SFA 模型 (Traditional SFA)
- SFA 的模型设定和估计方法
- 异质性 SFA 模型 (Heterogeneity SFA)
  - 模型设定
  - 结果解读和效率估计
- 面板 SFA
  - Grenne 难题
  - Greene ([2005](https://quqi.gblhgk.com/s/880197/Ehqseke3eZKpeRvt)), True Fixed Effect (TFE-SFA) 模型
  - [Wang and Ho (2010)](https://quqi.gblhgk.com/s/880197/LUEfv3T5CcJom8Rg), SP-SFA 模型
- 双边 SFA 模型 (Two-tier SFA)
  - MLE 估计方法
  - 效率估算方法
  - **范例：** 卢洪友, 连玉君, 卢盛峰. 中国医疗服务市场中的信息不对称程度测算. **经济研究**, 2011(4): 94-106. [[PDF]](https://quqi.gblhgk.com/s/880197/IEoXIU9Kqein0EQh)
- 内生性 SFA 模型 (Endog-SFA, Karakaplan and Kutlu, 2013，[[PDF]](https://quqi.gblhgk.com/s/880197/FEKQttsjbhCV9COz))

**参考文献：[PDF原文](https://quqi.gblhgk.com/s/880197/BlEjhDGozwNvE8ZK)**

- Greene, W., **2005**, Fixed and random effects in stochastic frontier models, **Journal of Productivity Analysis**, 23 (1): 7-32. [[PDF]](https://quqi.gblhgk.com/s/880197/1hauruPAFbLxwES2)
- Kumbhakar, S., E. Tsionas, T. Sipil inen, **2009**, Joint estimation of technology choice and technical efficiency: An application to organic and conventional dairy farming, **Journal of Productivity Analysis**, 31 (3): 151-161. [[PDF]](https://quqi.gblhgk.com/s/880197/Pd1RGZy9L8IRdvAo)
- Kumbhakar, S. C., H. J. Wang, A. P. Horncastle, **2015**, A practitioner's guide to stochastic frontier analysis using stata, **Cambridge University Press**. [Link](http://www.cambridge.org/us/academic/subjects/economics/econometrics-statistics-and-mathematical-economics/practitioners-guide-stochastic-frontier-analysis-using-stata#zB81hXruYSwDM5JQ.97)
- Kumbhakar, S. C., C. F. Parmeter, V. Zelenyuk, 2017, Stochastic frontier analysis: Foundations and advances. Working paper. [[PDF]](https://quqi.gblhgk.com/s/880197/Tk0Bw4svA5vL6x9K)
- Habib, M., A. Ljungqvist, **2005**, Firm value and managerial incentives: A stochastic frontier approach, **Journal of Business**, 78 (6): 2053-2094. [[PDF]](https://quqi.gblhgk.com/s/880197/0CJUWh3EXgxm5AzP)
- Wang, H. J., C. W. Ho, **2010**, Estimating fixed-effect panel stochastic frontier models by model transformation, **Journal of Econometrics**, 157 (2): 286-296. [[PDF]](https://quqi.gblhgk.com/s/880197/bUPcGhxaLZpOxQmk)
- 卢洪友, 连玉君, 卢盛峰, **2011**, 中国医疗服务市场中的信息不对称程度测算, **经济研究**, (4): 94-106. [[PDF]](https://quqi.gblhgk.com/s/880197/Hkw41xtx1yytnWtk)
- Karakaplan, M. U., & Kutlu, L. (2015). Handling Endogeneity in Stochastic Frontier Analysis. Economics Bulletin, 37(2), 889–901. [[PDF]](https://quqi.gblhgk.com/s/880197/kVkjiCy2Z5s4dNM6)
- 任曙明, 吕镯, **2014**, 融资约束、政府补贴与全要素生产率——来自中国装备制造企业的实证研究, **管理世界**, (11): 10-23. [[PDF]](https://sci-hub.tw/10.19744/j.cnki.11-1235/f.2014.11.003)


![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/横条-远山05.jpg)


## 6. 报名和缴费信息

**主办方：** 太原君泉教育咨询有限公司     
**标准费用** (含报名费、材料费)：3300 元/人 (全价)      
**优惠方案**：       
- 三人及以上团购/连享会直播课老学员：9 折，2970 元
- 五人及以上团购/已充值连享会会员：8 折，2640 元
- 老学员优惠：8 折，2640 元 (老学员: 此前参加过连享会现场班的学员)
- **温馨提示：** 以上各项优惠不能叠加使用。    

**联系方式：**       
  - 邮箱：[wjx004@sina.com](wjx004@sina.com)
  - 电话 (微信同号)：王老师 18903405450 ；李老师 ‭18636102467

**[报名链接](http://junquan18903405450.mikecrm.com/FhWLhS2)：**     
>  [http://junquan18903405450.mikecrm.com/FhWLhS2](http://junquan18903405450.mikecrm.com/FhWLhS2)                
> 或 长按/扫描二维码报名：   

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/二维码_TE_报名150.png "连享会-效率分析专题报名")


**缴费方式**

> **方式1：对公转账**

- 户名：太原君泉教育咨询有限公司  
- 账号：35117530000023891 (山西省太原市晋商银行南中环支行)
- **温馨提示：** 对公转账时，请务必提供「**汇款人姓名-单位**」信息，以便确认。

> **方式2：扫码支付**

<img style="width: 200px" src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/20200201183103.png">

> **温馨提示：** 扫码支付后，请将「**付款记录**」截屏发给王老师-18903405450（微信同号）。



&emsp;

## 7. 诚聘助教

[诚聘课程助教 (6名)](https://www.wjx.top/jq/73021543.aspx)，截止时间：2020 年 5 月 1 日
        

> **扫码报名：** https://www.wjx.top/jq/73021543.aspx  

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/二维码_TE_助教招聘150.png "连享会-效率分析专题-助教招聘")

&emsp;

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/横条-三人.jpg)



&emsp;

---
>**关于我们**

- **Stata连享会** 由中山大学连玉君老师团队创办，定期分享实证分析经验。
- [直播间](http://lianxh.duanshu.com) 有很多视频课程，可以随时观看。
- [::连享会-主页::](https://www.lianxh.cn) 和 [::连享会-知乎专栏::](https://www.zhihu.com/people/arlionn/) 发布了 200 多篇推文，分为 **内生性** | **空间计量** | **时序面板** | **结果输出** | **交乘调节** 五类，主流方法介绍一目了然：DID, RDD, IV, GMM, FE, Probit 等。

---

![连享会主页  lianxh.cn](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会跑起来就有风400.png "连享会主页：lianxh.cn")


> 扫码加入连享会微信群，提问交流更方便

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会-学习交流微信群001-150.jpg)
